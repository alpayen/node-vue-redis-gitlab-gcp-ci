# Docker CI/CD project - simple ToDo application

Start in local :
`docker-compose up`

----
#### Ports used
| Service | Port |
| --- | :---: |
| Node API | 3000 |
| Redis | 6379 |
| Node VueJS | 8080 |
| Redis Commander | 8081 |

----
#### Versions
| Service | Version |
| --- | :---: |
| Node API | node:8 |
| Redis | latest |
| Node VueJS | node:8 |
| Redis Commander | latest |
| VuesJS | 2.5.17 |


## API docs
https://documenter.getpostman.com/view/3427665/Rztmron1